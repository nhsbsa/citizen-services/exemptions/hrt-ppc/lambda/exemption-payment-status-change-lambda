import {
  loggerWithContext,
  setCorrelationId,
} from "@nhsbsa/hrt-ppc-npm-logging";
import { SQSRecord } from "aws-lambda";
import { updateCertificateStatusToActive } from "./api-utils";

function validMessageAttributes(record: SQSRecord) {
  loggerWithContext().info(`Validating SQS message attributes`);

  const { eventName, source, correlationId } = record.messageAttributes;
  const eventNameValue = eventName ? eventName.stringValue : undefined;
  const sourceValue = source ? source.stringValue : undefined;
  const correlationIdValue = correlationId
    ? correlationId.stringValue
    : undefined;

  loggerWithContext().info(
    `Received [eventName: ${eventNameValue}, source: ${sourceValue}, correlationId: ${correlationIdValue}]`,
  );
  if (!correlationIdValue) {
    const message =
      "Invalid SQS message attributes, must contain correlationId";
    loggerWithContext().error(message);
    throw new Error(message);
  }
  setCorrelationId(correlationIdValue);

  return correlationIdValue;
}

function validRecordbody(record: SQSRecord) {
  try {
    loggerWithContext().info(`Validating record body`);

    const { certificateId, paymentId, certificateType, paymentStatus } =
      JSON.parse(record.body);

    loggerWithContext().info(
      `Received [certificateId: ${certificateId}, paymentId: ${paymentId}, certificateType: ${certificateType}, paymentStatus: ${paymentStatus}]`,
    );

    if (!certificateId || !paymentId || !certificateType || !paymentStatus) {
      const message = `Invalid record body, must contain certificateId, paymentId, certificateType and paymentStatus`;
      loggerWithContext().error(message);
      throw new Error(message);
    }
    // Case sensitive
    if ("SUCCESS" !== paymentStatus) {
      loggerWithContext().info(
        `No need to update certificate status, [certificateId: ${certificateId}, paymentId: ${paymentId}, paymentStatus: ${paymentStatus}]`,
      );
      return;
    }
    return {
      certificateId,
      paymentId,
      certificateType,
      paymentStatus,
    };
  } catch (err) {
    const message = `Unable to parse record body: ${JSON.stringify(err)}`;
    loggerWithContext().error(message);
    throw err;
  }
}

export async function processRecord(record: SQSRecord) {
  loggerWithContext().info(`Processing event [messageId: ${record.messageId}]`);
  const correlationId = validMessageAttributes(record);
  const recordBody = validRecordbody(record);

  if (recordBody && correlationId) {
    const { certificateId, paymentId } = recordBody;

    const headers = {
      channel: "BATCH",
      "user-id": "PAYMENT_EVENT",
      "correlation-id": correlationId,
    };

    try {
      loggerWithContext().info(
        `Patching certificate status to ACTIVE [certificateId: ${certificateId}, paymentId: ${paymentId}]`,
      );
      await updateCertificateStatusToActive({
        certificateId,
        headers,
      });
      loggerWithContext().info(
        `Certificate record updated successfully [paymentId: ${paymentId}, certificateId: ${certificateId}]`,
      );
    } catch (error) {
      loggerWithContext().error(
        `Certificate record has not been updated [paymentId: ${paymentId}, certificateId: ${certificateId}]`,
      );
      throw error;
    }
  }
}
