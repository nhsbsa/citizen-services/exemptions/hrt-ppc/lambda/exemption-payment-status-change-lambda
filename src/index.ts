import { SQSEvent } from "aws-lambda";
import { logger } from "@nhsbsa/hrt-ppc-npm-logging";
import { processRecord } from "./process-sqs-event-record";

export async function handler(event: SQSEvent) {
  try {
    logger.info("exemption-payment-status-change-lambda");
    const { Records: records } = event;

    logger.info(`Events received count [${records.length}]`);

    await Promise.all(records.map((record) => processRecord(record)));
    return "Processing finished";
  } catch (err) {
    const message =
      JSON.stringify(err) + "\r\n All records in this batch will be retried";
    logger.error(message);

    throw err;
  }
}
