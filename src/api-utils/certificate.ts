import { CertificateApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";

const ACTIVE_STATUS = "ACTIVE";
const certificateApi = new CertificateApi();

export async function updateCertificateStatusToActive({
  certificateId,
  headers,
}) {
  await certificateApi.makeRequest({
    method: "PATCH",
    url: "/v1/certificates/" + certificateId,
    headers,
    data: {
      status: ACTIVE_STATUS,
    },
    responseType: "json",
  });
}
