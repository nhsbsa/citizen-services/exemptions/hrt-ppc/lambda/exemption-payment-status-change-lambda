import { updateCertificateStatusToActive } from "../certificate";
import { CertificateApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
let certificateApiSpy: jest.SpyInstance;

const certificateId = "certificateId";

const headers = {
  header: "value",
};
beforeEach(() => {
  certificateApiSpy = jest
    .spyOn(CertificateApi.prototype, "makeRequest")
    .mockResolvedValue({});
});
afterEach(() => {
  jest.resetAllMocks();
});

describe("updateCertificateStatusToActive()", () => {
  it("should not throw any error", async () => {
    expect(() =>
      updateCertificateStatusToActive({ certificateId, headers }),
    ).not.toThrow();
  });

  it("should call the certificate api with correct parameters", async () => {
    await updateCertificateStatusToActive({ certificateId, headers });
    expect(certificateApiSpy).toHaveBeenCalledTimes(1);
    expect(certificateApiSpy.mock.calls).toMatchInlineSnapshot(`
        [
          [
            {
              "data": {
                "status": "ACTIVE",
              },
              "headers": {
                "header": "value",
              },
              "method": "PATCH",
              "responseType": "json",
              "url": "/v1/certificates/certificateId",
            },
          ],
        ]
      `);
  });
});
