import { processRecord } from "../process-sqs-event-record";
import { mockSQSRecord } from "../__mocks__/mockData";
import * as certificate from "../api-utils/certificate";

let patchCertificateRecordSpy: jest.SpyInstance;

let sqsRecord;
beforeEach(() => {
  sqsRecord = { ...mockSQSRecord };
  patchCertificateRecordSpy = jest
    .spyOn(certificate, "updateCertificateStatusToActive")
    .mockResolvedValue();
});
afterEach(() => {
  jest.resetAllMocks();
  jest.restoreAllMocks();
});

describe("processRecord()", () => {
  it("should throw the error from patch certificate api", async () => {
    jest
      .spyOn(certificate, "updateCertificateStatusToActive")
      .mockRejectedValue(new Error("Api error"));
    await expect(() => processRecord(sqsRecord)).rejects.toThrow(
      new Error("Api error"),
    );
  });

  it("should process the sqs record successfully without any error", async () => {
    await expect(processRecord(sqsRecord)).resolves.not.toThrowError();
    expect(patchCertificateRecordSpy).toBeCalledTimes(1);
    expect(patchCertificateRecordSpy.mock.calls[0][0]).toMatchInlineSnapshot(`
      {
        "certificateId": "ac176001-8627-1a4b-8186-276bc4970000",
        "headers": {
          "channel": "BATCH",
          "correlation-id": "3ec1ee79-6860-4825-a733-acbd6b35796b",
          "user-id": "PAYMENT_EVENT",
        },
      }
    `);
  });

  it("should throw an error when sqs record body is empty", async () => {
    sqsRecord.body = JSON.stringify({});
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error(
        "Invalid record body, must contain certificateId, paymentId, certificateType and paymentStatus",
      ),
    );
    expect(patchCertificateRecordSpy).toBeCalledTimes(0);
  });

  it("should throw an error when sqs record body is unable to parse", async () => {
    sqsRecord.body = "This string unable to parse as object";
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error("Unexpected token T in JSON at position 0"),
    );
    expect(patchCertificateRecordSpy).toBeCalledTimes(0);
  });

  it("should not process with any api call when paymentStatus is FAILED", async () => {
    sqsRecord.body = JSON.stringify({
      certificateId: "ac176001-8627-1a4b-8186-276bc4970000",
      paymentId: "c0a8003b-8679-1571-8186-79f5d4000000",
      certificateType: "HRT_PPC",
      paymentStatus: "FAILED",
    });
    await processRecord(sqsRecord);
    expect(patchCertificateRecordSpy).toBeCalledTimes(0);
  });

  it("should not process with any api call when paymentStatus is in lower case", async () => {
    sqsRecord.body = JSON.stringify({
      certificateId: "ac176001-8627-1a4b-8186-276bc4970000",
      paymentId: "c0a8003b-8679-1571-8186-79f5d4000000",
      certificateType: "HRT_PPC",
      paymentStatus: "success",
    });
    await processRecord(sqsRecord);
    expect(patchCertificateRecordSpy).toBeCalledTimes(0);
  });

  it("should not process with any api call when paymentStatus is missing", async () => {
    sqsRecord.body = JSON.stringify({
      certificateId: "ac176001-8627-1a4b-8186-276bc4970000",
      paymentId: "c0a8003b-8679-1571-8186-79f5d4000000",
      certificateType: "HRT_PPC",
    });
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error(
        "Invalid record body, must contain certificateId, paymentId, certificateType and paymentStatus",
      ),
    );
    expect(patchCertificateRecordSpy).toBeCalledTimes(0);
  });

  it("should not process with any api call when certificateId is missing", async () => {
    sqsRecord.body = JSON.stringify({
      paymentId: "c0a8003b-8679-1571-8186-79f5d4000000",
      certificateType: "HRT_PPC",
      paymentStatus: "SUCCESS",
    });
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error(
        "Invalid record body, must contain certificateId, paymentId, certificateType and paymentStatus",
      ),
    );
    expect(patchCertificateRecordSpy).toBeCalledTimes(0);
  });

  it("should not process with any api call when paymentId is missing", async () => {
    sqsRecord.body = JSON.stringify({
      certificateId: "ac176001-8627-1a4b-8186-276bc4970000",
      certificateType: "HRT_PPC",
      paymentStatus: "SUCCESS",
    });
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error(
        "Invalid record body, must contain certificateId, paymentId, certificateType and paymentStatus",
      ),
    );
    expect(patchCertificateRecordSpy).toBeCalledTimes(0);
  });

  it("should not process with any api call when certificateType is missing", async () => {
    sqsRecord.body = JSON.stringify({
      certificateId: "ac176001-8627-1a4b-8186-276bc4970000",
      paymentId: "c0a8003b-8679-1571-8186-79f5d4000000",
      paymentStatus: "SUCCESS",
    });
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error(
        "Invalid record body, must contain certificateId, paymentId, certificateType and paymentStatus",
      ),
    );
    expect(patchCertificateRecordSpy).toBeCalledTimes(0);
  });

  it("should not process with any api call when correlationId stringValue is missing", async () => {
    delete sqsRecord.messageAttributes.correlationId.stringValue;

    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error("Invalid SQS message attributes, must contain correlationId"),
    );
    expect(patchCertificateRecordSpy).toBeCalledTimes(0);
  });

  it("should not process with any api call when correlationId is missing", async () => {
    delete sqsRecord.messageAttributes.correlationId;

    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error("Invalid SQS message attributes, must contain correlationId"),
    );
    expect(patchCertificateRecordSpy).toBeCalledTimes(0);
  });
});
